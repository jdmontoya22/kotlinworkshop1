import java.lang.IllegalArgumentException
import kotlin.math.*

/**
 * @author sigmotoa
 */

class Geometric {
    // Calculate the area for a square using one side
    fun squareArea(side: Int): Int {
        if(side < 0) {
            throw IllegalArgumentException()
        } else {
            return (side.toDouble().pow(2.0).toInt())
        }

        // Prueba de intento para valores Negativos
        /* try {
            if(side > 0) {
                return (side.toDouble().pow(2.0).toInt())
            }
        } catch(e: IllegalArgumentException) {
            throw e
        }*/
    }

    // Calculate the area for a square using two sides
    fun squareArea(sideA: Int, sideB: Int): Int {
        if(sideA < 0 && sideB < 0) {
            throw IllegalArgumentException()
        } else {
            return (sideA * sideB)
        }
        // Prueba de valores negativos
    }

    // Calculate the area of circle with the radius
    fun circleArea(radius: Double): Double {
        if(radius < 0) {
            throw IllegalArgumentException()
        } else {
            return (PI * radius.pow(2.0))
        }
    }

    // Calculate the perimeter of circle with the diameter
    fun circlePerimeter(diameter: Int): Double {
        return (diameter.toDouble() * PI)
    }

    // Calculate the perimeter of square with a side
    fun squarePerimeter(side: Double): Double {
        return (side * 4)
    }

    // Calculate the volume of the sphere with the radius
    fun sphereVolume(radius: Double): Double {
        return ((4 * PI * radius.pow(3.0)) / 3)
    }

    // Calculate the area of regular pentagon with one side
    fun pentagonArea(side: Int): Float {
        val resultado = ((sqrt(5.00 * (5.00 + 2.00 * (sqrt(5.0).toFloat()))).toFloat() * side.toFloat() * side.toFloat()) / 4.00).toFloat()
        val final = (Math.round(resultado * 10.0) / 10.0).toFloat()
        return final
    }

    // Calculate the Hypotenuse with two legs
    fun calculateHypotenuse(legA: Double, legB: Double): Double {
        return (sqrt(legA.pow(2) + legB.pow(2)))
    }
}