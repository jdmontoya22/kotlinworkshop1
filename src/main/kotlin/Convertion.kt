/**
 * @author sigmotoa
 *
 * @version 1
 *
 * Convertion Exercises
 */
class Convertion {
    // convert of units of Metric System
    // Km to metters
    fun kmToM1(km: Double): Int {
        return ((km * 1000 / 1).toInt())
    }

    // Km to metters
    fun kmTom(km: Double): Double {
        return (km * (1000 / 1))
    }

    // Km to cm
    fun kmTocm(km: Double): Double {
        return (km * (1000 / 1) * (100 / 1))
    }

    // milimetters to metters
    fun mmTom(mm: Int): Double {
        return (mm.toDouble() * 1 / 1000)
    }

    // convert of units of U.S Standard System
    // convert miles to foot
    fun milesToFoot(miles: Double): Double {
        return (miles * (5280 / 1))
    }

    // convert yards to inches
    fun yardToInch(yard: Int): Int {
        return (yard * (36 / 10))
    }

    // convert inches to miles
    fun inchToMiles(inch: Double): Double {
        return (inch * (1.0 / 39.37) * (1.0 / 1000.0) * (0.621371 / 1.0))
    }

    // convert foot to yards
    fun footToYard(foot: Int): Int {
        return (foot / 3)
    }

    // Convert units in both systems
    // convert Km to inches
    fun kmToInch(km: String?): Double {
        val kmEnd = km?.toDoubleOrNull()
        return (kmEnd!! * (1000 / 1) * (100 / 1) * (1 / 2.54))
    }

    // convert milimmeters to foots
    fun mmToFoot(mm: String?): Double {
        val mmEnd = mm?.toDouble()
        return (mmEnd!! * (1 / 304.8))
    }

    // convert yards to cm
    fun yardToCm(yard: String?): Double {
        val yardEnd = yard?.toDouble()
        return (yardEnd!! * (0.9144 / 1) * (100 / 1))
    }
}