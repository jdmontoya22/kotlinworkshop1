/**
 * @author sigmotoa
 *
 * This class contains some popular brain games with numbers
 */


class Agility {
    // Bigger than
    // Show if the first number is bigger than the second
    fun biggerThan(numA: String, numB:String):Boolean {
        if (numA > numB) { return true }
        return false
    }

    // Sort from bigger the numbers an show in list
    fun order(numA:Int, numB:Int, numC:Int, numD:Int, numE:Int): List<Int?> {
        val myOrderedList = listOf(numA, numB, numC, numD, numE).sorted()
        return myOrderedList
    }

    // Look for the smaller number of the list
    fun smallerThan(list: List<Double>): Double {
        var small = Double.MAX_VALUE
        for(i in list) {
            small = small.coerceAtMost(i)
        }
        return small
    }

    // Palindrome number is called in Spanish capicúa
    // The number is palindrome
    fun palindromeNumber(numA: Int): Boolean {
        var suma = 0
        var tempNumber = numA
        while (tempNumber != 0 && tempNumber > 0) {
            val revision = tempNumber % 10
            suma = suma * 10 + revision
            tempNumber /= 10
        }
        // Validación del número despues de su proceso
        if (suma == numA) { return true }
        return false
    }

    //the word is palindrome?
    fun palindromeWord(word: String): Boolean {
        for(conteo in word.length -1 downTo 0){
            if(word[conteo] != word[word.length -1 -conteo]){ return false }
        }
        return true
    }

    // Show the factorial number for the parameter
    fun factorial(numA: Int):Int {
        var answer = 1
        var factorial: Int = numA
        while (factorial > 1) {
            answer *= factorial
            factorial--
        }
        return answer
    }

    //is the number odd
    fun is_Odd(numA: Byte): Boolean {
        if (numA % 2 != 0) { return true }
        return false
    }

    // is the number prime
    fun isPrimeNumber(numA:Int): Boolean {
        if(numA < 2) { return false }
        for(i in 2 .. numA / 2) {
            if(numA % i == 0) { return false }
        }
        return true
    }

    //is the number even
    fun is_Even(numA: Byte): Boolean {
        if(numA % 2 == 0) { return true }
        return false
    }
    //is the number perfect
    fun isPerfectNumber(numA:Int): Boolean  {
        var suma = 0
        for(i in 1 .. numA / 2) {
            if(numA % i == 0) {
                suma += i
            }
        }
        if(suma == numA) { return true }
        return false
    }

    // Return an array with the fibonacci sequence for the requested number
    fun fibonacci(numA: Int): List<Int?> {
        var fib = 0
        var aux = 1
        val fibo=mutableListOf<Int>()
        if(numA > 0) {
            (0 .. numA).forEach {
                fibo.add(fib)
                aux += fib
                fib = aux - fib
            }
        }
        return  fibo
    }

    // how many times the number is divided by 3
    fun timesDividedByThree(numA: Int):Int {
        var cont = 0
        var aux = numA
        while(aux >= 3){
            cont=cont+1
            aux = aux-3
        }
        return cont
    }

    //The game of fizzbuzz
    fun fizzBuzz(numA: Int):String? {
        return when {
            (numA % 3 == 0 && numA % 5 == 0) -> return "FizzBuzz"
            (numA % 3 == 0) -> return "Fizz"
            (numA % 5 == 0) -> return "Buzz"
            else -> numA.toString()
        }

        /**
         * If number is divided by 3, show fizz
         * If number is divided by 5, show buzz
         * If number is divided by 3 and 5, show fizzbuzz
         * in other cases, show the number
         */
    }
}