import org.junit.Test

import org.junit.Assert.*

class ConvertionTest {

    private val converter = Convertion()
    @Test
    fun kmToM1() {
        assertEquals(178,converter.kmToM1(0.178))
    }

    @Test
    fun kmTom() {
        assertEquals(12452.0,converter.kmTom(12.452),0.0)
    }

    @Test
    fun kmTocm() {
        assertEquals(7630.0,converter.kmTocm(0.07630),0.000000000001)
        assertEquals(5907000.0,converter.kmTocm(59.07),0.0000000000001)
    }

    @Test
    fun mmTom() {
        assertEquals(3141.516,converter.mmTom(3141516),0.1)
    }

    @Test
    fun milesToFoot() {
        assertEquals(469920.0,converter.milesToFoot(89.0),0.0)
    }

    @Test
    fun yardToInch() {
        assertEquals(267,converter.yardToInch(89))
    }

    @Test
    fun inchToMiles() {
        // Verificación por parte del profesor, delta puede afectar
        assertEquals(0.0014678030303030302,converter.inchToMiles(93.0),0.0)
        assertEquals(1.4046717171717172E-5,converter.inchToMiles(0.89),0.0)
    }

    @Test
    fun footToYard() {
        assertEquals(316,converter.footToYard(950))
    }

    @Test
    fun kmToInch() {
        assertEquals(3748425.2,converter.kmToInch("95.21"),0.1)
    }

    @Test
    fun mmToFoot() {
        assertEquals(0.3123687,converter.mmToFoot("95.21"),0.1)
    }

    @Test
    fun yardToCm() {
        assertEquals(8706.0024,converter.yardToCm("95.21"),0.1)
    }

    @Test
    fun errors() {
        // Omisión
        assertEquals("Error", true, converter.kmToInch("Km"))
        assertEquals("Error", true, converter.mmToFoot("mm"))
        assertEquals("Error", true, converter.yardToCm("yard"))
    }
}